use super::*;
use CharacterVariant::*;
use Token::*;

#[cfg(test)]
pub fn lex(input: &str) -> Vec<Token> {
    let lexer = Lexer::new(input.chars());
    lexer.collect()
}

#[test]
fn simple_name() {
    let result = lex("Atom");
    println!("{:?}", result);
    assert_eq!(result.len(), 1);
    assert_eq!(result[0], Atom("Atom".to_string()));
}

#[test]
fn simple_string() {
    let result = lex("\"Atom\"");
    println!("{:?}", result);
    assert_eq!(result.len(), 1);
    assert_eq!(result[0], StringLiteral("Atom".to_string()));
}

#[test]
fn simple_number() {
    let result = lex("21");
    println!("{:?}", result);
    assert_eq!(result.len(), 1);
    assert_eq!(result[0], NumberLiteral("21".to_string()));
}

#[test]
fn special_characters() {
    let result = lex("==");
    println!("{:?}", result);
    assert_eq!(result.len(), 1);
    assert_eq!(result[0], Atom("==".to_string()));
}

#[test]
fn bracket_open() {
    let result = lex("(");
    println!("{:?}", result);
    assert_eq!(result.len(), 1);
    assert_eq!(result[0], Bracket(Left));
}

#[test]
fn bracket_close() {
    let result = lex(")");
    println!("{:?}", result);
    assert_eq!(result.len(), 1);
    assert_eq!(result[0], Bracket(Right));
}

#[test]
fn combined() {
    let result = lex("(Atom \"string\" 123 ())");
    println!("{:?}", result);
    assert_eq!(result.len(), 7);
    assert_eq!(result[0], Bracket(Left));
    assert_eq!(result[1], Atom("Atom".to_string()));
    assert_eq!(result[2], StringLiteral("string".to_string()));
    assert_eq!(result[3], NumberLiteral("123".to_string()));
    assert_eq!(result[4], Bracket(Left));
    assert_eq!(result[5], Bracket(Right));
    assert_eq!(result[6], Bracket(Right));
}
