pub mod lexemes;

#[cfg(test)]
mod test;

use crate::preallocation;
use lexemes::*;

use std::iter::Peekable;
use std::str::Chars;

type PeekableCharIter<'a> = Peekable<Chars<'a>>;

pub struct Lexer<'a> {
    input_stream: PeekableCharIter<'a>,
}

impl Iterator for Lexer<'_> {
    type Item = Token;

    fn next(&mut self) -> Option<Token> {
        use char_classes::*;
        use CharacterVariant::*;

        self.skip_whitespace();

        match self.input_stream.peek() {
            Some('(') => {
                self.input_stream.next();
                Some(Token::Bracket(Left))
            }
            Some(')') => {
                self.input_stream.next();
                Some(Token::Bracket(Right))
            }
            Some(c) if is_string_delimitor(*c) => Some(self.read_string()),
            Some(c) if is_initial_number_char(*c) => Some(self.read_number()),
            Some(c) if is_initial_atom_char(*c) => Some(self.read_atom()),
            Some(c) if c.is_whitespace() => panic!("no whitespace expected"),
            Some(_) => panic!("Unknown input character"),
            None => None,
        }
    }
}

impl Lexer<'_> {
    pub fn new(input: Chars) -> Lexer {
        Lexer {
            input_stream: input.peekable(),
        }
    }

    fn skip_whitespace(&mut self) {
        while let Some(true) = self.input_stream.peek().map(|c| c.is_whitespace()) {
            self.input_stream.next();
        }
    }

    fn read_number(&mut self) -> Token {
        use char_classes::*;

        let mut number = String::with_capacity(preallocation::CHARACTERS_PER_LEXEME);
        let initial_char = self.input_stream.next().expect("only call on nonempty");
        assert!(char_classes::is_initial_number_char(initial_char));
        number.push(initial_char);

        while let Some(true) = self.input_stream.peek().map(|c| is_number_char(*c)) {
            number.push(self.input_stream.next().unwrap());
        }

        Token::NumberLiteral(number)
    }

    fn read_atom(&mut self) -> Token {
        use char_classes::*;

        let mut atom = String::with_capacity(preallocation::CHARACTERS_PER_LEXEME);
        let initial_char = self.input_stream.next().expect("only call on nonempty");
        assert!(char_classes::is_initial_atom_char(initial_char));
        atom.push(initial_char);

        while let Some(true) = self.input_stream.peek().map(|c| is_atom_char(*c)) {
            atom.push(self.input_stream.next().unwrap());
        }

        Token::Atom(atom)
    }

    fn read_string(&mut self) -> Token {
        use char_classes::*;

        let mut string = String::with_capacity(preallocation::CHARACTERS_PER_STRING_LITERAL);

        let initial_delimitor = self.input_stream.next().expect("only call on nonempty");
        assert!(is_string_delimitor(initial_delimitor));

        while let Some(false) = self.input_stream.peek().map(|c| is_string_delimitor(*c)) {
            string.push(self.input_stream.next().unwrap());
        }

        let end_delimitor = self.input_stream.next().expect("only call on nonempty");
        assert!(is_string_delimitor(end_delimitor));

        Token::StringLiteral(string)
    }
}

mod char_classes {
    pub fn is_initial_atom_char(c: char) -> bool {
        (c.is_alphabetic() || c.is_ascii_punctuation()) && !is_string_delimitor(c)
    }

    pub fn is_atom_char(c: char) -> bool {
        c.is_alphanumeric() || c.is_ascii_punctuation() || c.is_ascii_digit()
    }

    pub fn is_initial_number_char(c: char) -> bool {
        c.is_numeric()
    }

    pub fn is_number_char(c: char) -> bool {
        is_initial_number_char(c)
    }

    pub fn is_string_delimitor(c: char) -> bool {
        c == '"'
    }
}
