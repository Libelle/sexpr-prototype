use std::fmt::*;

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum CharacterVariant {
    Left,
    Right,
}

#[derive(Debug, PartialEq, Clone)]
pub enum Token {
    Bracket(CharacterVariant),
    Atom(String),
    NumberLiteral(String),
    StringLiteral(String),
}

impl Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use CharacterVariant::*;
        use Token::*;

        let string = match self {
            Bracket(Left) => "(".to_string(),
            Bracket(Right) => ")".to_string(),
            Atom(identifier) => identifier.to_string(),
            NumberLiteral(number) => number.to_string(),
            StringLiteral(string) => format!("\"{}\"", string),
        };

        write!(f, "{}", string)
    }
}
