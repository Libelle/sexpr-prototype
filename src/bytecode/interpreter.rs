use super::Instruction::*;
use super::*;

mod tunables {
    pub const INITIAL_STACK_SIZE: usize = 128;
}

struct InterpreterState {
    ip: usize,
    stack: Vec<i64>,
}

impl InterpreterState {
    fn new() -> Self {
        Self {
            ip: 0,
            stack: Vec::with_capacity(tunables::INITIAL_STACK_SIZE),
        }
    }
}

pub fn interpret(bytecode: &Bytecode) -> i64 {
    let mut state = InterpreterState::new();

    macro_rules! arg {
        () => {
            state
                .stack
                .pop()
                .expect("interpret: tried to pop on empty stack")
        };
    }

    macro_rules! res {
        ($x:expr) => {
            state.stack.push($x)
        };
    }

    loop {
        let raw_instruction = &bytecode.instructions[state.ip];
        let instruction = unsafe { raw_instruction.instruction };

        let mut step_size = 1;

        match instruction {
            Dup => {
                let top = arg!();
                res!(top);
                res!(top);
            }
            Pop => {
                let _ = arg!();
            }
            IntegerZero => {
                res!(0);
            }
            IntegerOne => {
                res!(1);
            }
            IntegerConstant => {
                // TODO: This is far from ideal...
                step_size += 8; // Data of size 8;
                let raw_data = &bytecode.instructions[state.ip + 1..state.ip + 1 + 8];
                let raw_u8s: Vec<u8> = raw_data
                    .iter()
                    .map(unsafe { |a: &InstrData| a.raw_data })
                    .collect();

                let data = i64::from_le_bytes(raw_u8s.try_into().expect("aaa"));
                state.stack.push(data);
            }

            IntegerAdd => {
                let x = arg!();
                let y = arg!();
                res!(y + x);
            }
            IntegerSub => {
                let x = arg!();
                let y = arg!();
                res!(y - x);
            }
            IntegerMul => {
                let x = arg!();
                let y = arg!();
                res!(y * x);
            }
            IntegerDiv => {
                let x = arg!();
                let y = arg!();
                if x == 0 {
                    panic!("interpret: div: division by 0");
                }
                res!(y / x);
            }
            IntegerEquals => {
                let x = arg!();
                let y = arg!();
                res!((y == x) as i64);
            }
            _ => panic!("bytecode-interpreter: unknown instruction"),
        };

        state.ip += step_size;
        if state.ip >= bytecode.instructions.len() {
            break;
        }
    }

    return *state.stack.last().expect("interpret: no result on stack");
}

/*
#[test]
fn test_interpret_recursive() {
    let code = Bytecode {
        instructions: vec![
            IntegerOne,
            IntegerOne,
            IntegerAdd,
            IntegerOne,
            IntegerOne,
            IntegerAdd,
            IntegerMul,
        ],
       };


    let result = interpret(&code);
    assert_eq!(result, 4);
}

#[test]
fn test_interpret_mul() {
    let code = Bytecode {
        instructions: vec![
            IntegerZero,
            IntegerOne,
            IntegerMul,
        ],
       };


    let result = interpret(&code);
    assert_eq!(result, 0);
}

#[test]
fn test_interpret_add() {
    let code = Bytecode {
        instructions: vec![
            IntegerZero,
            IntegerOne,
            IntegerAdd,
        ],
       };


    let result = interpret(&code);
    assert_eq!(result, 1);
}

#[test]
fn test_interpret_add_ones() {
    let code = Bytecode {
        instructions: vec![
            IntegerOne,
            IntegerOne,
            IntegerAdd,
        ],
       };


    let result = interpret(&code);
    assert_eq!(result, 2);
}
*/
