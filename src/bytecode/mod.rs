use crate::codegen;
use core::fmt::Debug;
use core::fmt::Formatter;

pub mod interpreter;

#[repr(u8)]
#[derive(Debug, Clone, Copy)]
#[allow(dead_code)]
pub enum Instruction {
    Dup,
    Pop,
    BoolAnd,
    BoolOr,
    BoolXor,
    BoolNot,

    IntegerZero,
    IntegerOne,
    IntegerConstant,

    IntegerAdd,
    IntegerSub,
    IntegerMul,
    IntegerDiv,

    IntegerEquals,
}

#[repr(C)]
pub union InstrData {
    instruction: Instruction,
    raw_data: u8,
}

impl Debug for InstrData {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "n.a.")
    }
}

#[derive(Debug)]
pub struct Bytecode {
    instructions: Vec<InstrData>,
}

impl Bytecode {
    pub fn new() -> Bytecode {
        Bytecode {
            instructions: Vec::with_capacity(codegen::tunables::INITIAL_BYTECODE_SIZE),
        }
    }

    pub fn push(&mut self, instruction: Instruction) {
        self.instructions.push(InstrData { instruction });
    }

    pub fn push_data(&mut self, data: [u8; 8]) {
        for ele in data {
            self.instructions.push(InstrData { raw_data: ele });
        }
    }
}
