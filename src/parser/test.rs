use crate::lexer::lexemes::*;
use CharacterVariant::*;
use Token::*;

use super::ast::SExpression::*;
use super::*;

#[test]
fn empty_list() {
    let input = vec![Bracket(Left), Bracket(Right)];

    let result = parse(input);
    assert_eq!(result, List(Vec::new()));
}

#[test]
fn nested_lists() {
    let input = vec![
        Bracket(Left),
        Bracket(Left),
        Bracket(Left),
        Bracket(Left),
        Bracket(Right),
        Bracket(Right),
        Bracket(Right),
        Bracket(Left),
        Bracket(Right),
        Bracket(Left),
        Bracket(Right),
        Bracket(Right),
    ];

    let result = parse(input);

    assert_eq!(
        result,
        List(vec![
            List(vec![List(vec![List(Vec::new())])]),
            List(Vec::new()),
            List(Vec::new()),
        ])
    );
}

#[test]
fn atom() {
    let input = vec![Token::Atom("atom".to_string())];

    let result = parse(input);
    assert_eq!(result, SExpression::Atom("atom".to_string()));
}

#[test]
fn number() {
    let input = vec![Token::NumberLiteral("123".to_string())];
    let result = parse(input);
    assert_eq!(result, SExpression::IntegerLiteral(123));
}

#[test]
fn string() {
    let input = vec![Token::StringLiteral("string".to_string())];
    let result = parse(input);
    assert_eq!(result, SExpression::StringLiteral("string".to_string()));
}

#[test]
fn combined() {
    let input = vec![
        Bracket(Left),
        Token::Atom("+".to_string()),
        Token::NumberLiteral("100".to_string()),
        Token::StringLiteral("hello".to_string()),
        Bracket(Left),
        Bracket(Right),
        Bracket(Right),
    ];

    let result = parse(input);
    assert_eq!(
        result,
        List(vec![
            SExpression::Atom("+".to_string()),
            SExpression::IntegerLiteral(100),
            SExpression::StringLiteral("hello".to_string()),
            List(Vec::new()),
        ])
    );
}
