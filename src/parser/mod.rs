pub mod ast;

#[cfg(test)]
mod test;

use ast::*;

use crate::lexer::lexemes::*;
use crate::preallocation;
use CharacterVariant::*;
use Token::*;

pub fn parse(tokens: Vec<Token>) -> SExpression {
    let mut state = ParseState::new(&tokens);
    let parsetree = sexpression(&mut state);
    state.expect_end();

    parsetree
}

fn sexpression(state: &mut ParseState) -> SExpression {
    match state.peek() {
        Bracket(Left) => list(state),
        Atom(_) => atom(state),
        NumberLiteral(_) => number(state),
        StringLiteral(_) => string(state),
        _ => panic!("parser: unexpected token;"),
    }
}

fn list(state: &mut ParseState) -> SExpression {
    let mut list_content = Vec::with_capacity(preallocation::ELEMENTS_PER_SEXPRESSION);

    state.accept(Bracket(Left));

    while state.peek() != Bracket(Right) {
        let subexpression = sexpression(state);
        list_content.push(subexpression);
    }

    state.accept(Bracket(Right));

    SExpression::List(list_content)
}

fn atom(state: &mut ParseState) -> SExpression {
    if let Atom(identifier) = state.peek() {
        // Note: this double access is not particularly beautiful,
        // but I considered it better than being able to directly
        // manipulate the ParseState index from the outside.
        state.accept(Atom(String::new()));
        return SExpression::Atom(identifier);
    }

    panic!("parser-internal: Expected atom here;");
}

fn number(state: &mut ParseState) -> SExpression {
    if let NumberLiteral(number_string) = state.peek() {
        // Note: this double access is not particularly beautiful,
        // but I considered it better than being able to directly
        // manipulate the ParseState index from the outside.
        state.accept(NumberLiteral(String::new()));
        if let Ok(value) = number_string.parse::<i64>() {
            return SExpression::IntegerLiteral(value);
        } else {
            panic!("parser: failed to convert numeral to i64;");
        }
    }

    panic!("parser-internal: expected numeral here;");
}

fn string(state: &mut ParseState) -> SExpression {
    if let StringLiteral(string_string) = state.peek() {
        // Note: this double access is not particularly beautiful,
        // but I considered it better than being able to directly
        // manipulate the ParseState index from the outside.
        state.accept(StringLiteral(String::new()));
        return SExpression::StringLiteral(string_string);
    }

    panic!("parser-internal: expected numeral here;");
}
struct ParseState<'a> {
    tokens: &'a Vec<Token>,
    index: usize,
}

impl ParseState<'_> {
    fn new(tokens: &Vec<Token>) -> ParseState {
        ParseState { tokens, index: 0 }
    }
}

impl ParseState<'_> {
    fn accept(&mut self, expected_token: Token) {
        let next_token = self.peek();

        let is_matching = match (expected_token.clone(), next_token) {
            (Bracket(a), Bracket(b)) => a == b,
            (Atom(_), Atom(_)) => true,
            (NumberLiteral(_), NumberLiteral(_)) => true,
            (StringLiteral(_), StringLiteral(_)) => true,
            _ => false,
        };

        if is_matching {
            self.index += 1;
        } else {
            panic!("parser: unexpected symbol {:?}", expected_token);
        }
    }

    fn peek(&mut self) -> Token {
        if self.index >= self.tokens.len() {
            panic!("parser: got EOF, expected further tokens;")
        }

        self.tokens[self.index].clone()
    }

    fn expect_end(&self) {
        if self.tokens.len() != self.index {
            panic!("parser: expected EOF, got {:?};", self.tokens[self.index]);
        }
    }
}
