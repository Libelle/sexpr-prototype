use std::fmt::Display;

#[derive(Debug, PartialEq)]
pub enum SExpression {
    List(Vec<SExpression>),
    Atom(String),
    IntegerLiteral(i64),
    StringLiteral(String),
}

impl Display for SExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use SExpression::*;

        match self {
            List(list) => {
                write!(f, "(")?;

                if !list.is_empty() {
                    list[0].fmt(f)?;
                }

                for e in &list[1..] {
                    write!(f, " ")?;
                    e.fmt(f)?;
                }

                write!(f, ")")?;
            }
            Atom(identifier) => {
                write!(f, "{}", identifier)?;
            }
            IntegerLiteral(number) => {
                write!(f, "{}", number)?;
            }
            StringLiteral(string) => {
                write!(f, "\"{}¸\"", string)?;
            }
        }

        Ok(())
    }
}
