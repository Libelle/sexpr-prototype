use crate::bytecode::*;
use crate::parser::ast::SExpression;

use Instruction::*;
use SExpression::*;

pub mod tunables {
    pub const INITIAL_BYTECODE_SIZE: usize = 1048; // 1kB
}

pub fn codegen(ast: &SExpression) -> Bytecode {
    let mut bytecode = Bytecode::new();
    codegen_expression(ast, &mut bytecode);
    bytecode
}

fn codegen_expression(ast: &SExpression, bytecode: &mut Bytecode) {
    match ast {
        List(elements) => codegen_funcall(elements, bytecode),
        IntegerLiteral(val) => codegen_numeral(*val, bytecode),
        Atom(_) => panic!("bytecode-codegen: Atoms / Variables not implemented yet."),
        StringLiteral(_) => panic!("bytecode-codegen: String literals not implemented yet."),
    }
}

fn codegen_numeral(val: i64, bytecode: &mut Bytecode) {
    match val {
        0 => bytecode.push(IntegerZero),
        1 => bytecode.push(IntegerOne),
        val => {
            bytecode.push(IntegerConstant);
            bytecode.push_data(val.to_le_bytes());
        }
    }
}

// TODO: Extract the different operations better,
// TODO: Handle the 0-argument cases for add and mul,
// TODO: Add asserts to check for argument amount
fn codegen_funcall(elements: &Vec<SExpression>, bytecode: &mut Bytecode) {
    if let Atom(function_name) = &elements[0] {
        for subexpression in &elements[1..] {
            codegen_expression(subexpression, bytecode);
        }

        let op_code = match function_name.as_str() {
            "+" => IntegerAdd,
            "-" => IntegerSub,
            "*" => IntegerMul,
            "/" => IntegerDiv,
            "==" => IntegerEquals,
            _ => panic!("bytecode-codegen: unknown function '{function_name}'"),
        };

        // we need to generate more mul / add instructions,
        // if we have more arguments.
        // 1 less for starting element being the instruction,
        // 1 less because adding takes 2 arguments
        for _ in 0..elements.len() - 2 {
            bytecode.push(op_code);
        }
    } else {
        panic!("bytecode-codegen: we do not support function expressions")
    }
}
