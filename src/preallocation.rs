pub const CHARACTERS_PER_LEXEME: usize = 16;
pub const CHARACTERS_PER_STRING_LITERAL: usize = 32;
pub const ELEMENTS_PER_SEXPRESSION: usize = 8;
