use bytecode::Bytecode;
use lexer::Lexer;

mod bytecode;
mod codegen;
mod lexer;
mod parser;
mod preallocation;

fn main() {
    let result = compile_and_run("(+ 1 (* 3 3 4))");
    println!("Result: {}", result);
}

fn compile_and_run(source: &str) -> i64 {
    let bytecode = compile(source);
    run(&bytecode)
}

fn compile(source: &str) -> Bytecode {
    let input_stream = source.chars();
    let lexer = Lexer::new(input_stream);
    let lexemes = lexer.collect();
    let parsetree = parser::parse(lexemes);
    codegen::codegen(&parsetree)
}

fn run(bytecode: &Bytecode) -> i64 {
    bytecode::interpreter::interpret(bytecode)
}

#[test]
fn test_integer_literals() {
    let result = compile_and_run("(- (+ 32 1) (+ 1 31))");
    assert_eq!(result, 1);
}
#[test]
fn test_add_mul() {
    let result = compile_and_run("(* (+ 1 1) (+ 1 1))");
    assert_eq!(result, 4);
}

#[test]
fn test_add_three_args() {
    let result = compile_and_run("(+ 1 1 1)");
    assert_eq!(result, 3);
}

#[test]
fn test_equality_true() {
    let result = compile_and_run("(== (+ 1 1) (+ 1 1))");
    assert_eq!(result, 1);
}

#[test]
fn test_equality_false() {
    let result = compile_and_run("(== (+ 1 1) (+ 1 0))");
    assert_eq!(result, 0);
}
