[![status-badge](https://ci.codeberg.org/api/badges/12735/status.svg)](https://ci.codeberg.org/repos/12735)

# S-Expression Compiler Prototype

This is a prototype for the Libelle language and it's Larve bytecode.

## Current State

It is very much in an experimental state, do not expect it to be useful yet!
Though you are very much welcome to take a look at the code, if you are interested in compilers.

If you have features or would like to join the work, please just open an issue.

## Future Plans

Once the code is sufficiently matured and the bytecode is somewhat stable,
this repo will be split up and moved into the Libelle and Larve Repositories
